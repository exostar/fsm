#ifndef _ZBX_FSM_HPP
#define _ZBX_FSM_HPP

#include <utility>
#include <functional>
#include <unordered_map>
#include <initializer_list>

namespace zbx::fsm
{

template<class EEvent, class Context>
class state_machine;

template<class EEvent, class Context>
class event_coordinator
{
public:
	using Self = event_coordinator<EEvent, Context>;

	using StateMachine = state_machine<EEvent, Context>;
	using Handler = std::function<void(StateMachine&,Context&)>;	
	using HandlerMap = std::unordered_map<EEvent, Handler>;

	event_coordinator() = default;
	inline event_coordinator(const Self& e) : m_handlers(e.m_handlers) { }
	inline Self& operator=(const Self& e) { m_handlers = e.m_handlers; return *this; }
	inline event_coordinator(Self&& e) noexcept : m_handlers(std::move(e.m_handlers)) { }
	inline Self& operator=(Self&& e) noexcept { m_handlers = std::move(e.m_handlers); return *this; }

	~event_coordinator() = default;

	template<class ...Ts> inline event_coordinator(Ts... ev_hndlr_pairs) : m_handlers({ev_hndlr_pairs...}) { }
	inline event_coordinator(std::initializer_list<typename HandlerMap::value_type> ev_hndlr_pairs) : m_handlers(ev_hndlr_pairs) { }

	inline bool exists(const EEvent ev) const { return m_handlers.find(ev) != m_handlers.end(); }
	inline bool empty() const { return m_handlers.empty(); }
	inline bool handle(const EEvent ev, StateMachine& sm, Context& ctx) const { if (auto found = m_handlers.find(ev); found != m_handlers.end()) { found->second(sm, ctx); return true; } else return false; }

	inline Handler& operator[](const EEvent ev) { return m_handlers[ev]; }
	inline Handler operator[](const EEvent ev) const { if (auto found = m_handlers.find(ev); found != m_handlers.end()) return found->second; else return Handler(); }
private:
	HandlerMap m_handlers;
};

template<class EEvent, class Context>
class state_machine
{
public:
	using Self = state_machine<EEvent, Context>;

	using EventCoordinator = event_coordinator<EEvent, Context>;

	state_machine() = default;
	inline state_machine(const Self& s) : m_status(s.m_status), m_running(s.m_running) { }
	inline Self& operator=(const Self& s) { m_status = s.m_status; m_running = s.m_running; return *this; }
	inline state_machine(Self&& s) noexcept : m_status(s.m_status), m_running(s.m_running) { s.m_running = false; }
	inline Self& operator=(Self&& s) noexcept { m_status = s.m_status; m_running = s.m_running; s.m_running = false; return *this; }

	~state_machine() = default;

	[[nodiscard]] inline bool active() const { return m_running; }
	inline void stop() { m_running = false; }
	[[nodiscard]] inline EEvent query() const { return m_status; }
	inline void post(const EEvent ev) { m_status = ev; }
	
	inline void run(const EventCoordinator& ec, Context& ctx) {
		m_running = true;
		while (m_running) { if (!ec.handle(m_status, *this, ctx)) break; }
		m_running = false;
	}
	inline void run(EEvent initial, const EventCoordinator& ec, Context& ctx) { m_status = initial; this->run(ec, ctx); }
private:
	EEvent m_status;
	bool m_running = false;
};

} // [end] namespace zbx::fsm

#endif // #ifndef _ZBX_FSM_HPP
