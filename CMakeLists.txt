add_library(zbx_fsm INTERFACE)
target_include_directories(zbx_fsm INTERFACE "${CMAKE_CURRENT_SOURCE_DIR}/include")
add_library(zbx_fsm::zbx_fsm ALIAS zbx_fsm)
